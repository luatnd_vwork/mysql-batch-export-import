#!/bin/bash

# Modify your database config in config.sh before run ./importDb.sh
source config-import.sh
source inc/mysql_utils.sh
source inc/notice.sh

if [ "$1" != "" ]; then
    echo "Do import only some files: " $@

    for filePath in $@; do 
        echo -e "\n"
        #echo -e "\nProcessing $filePath ../"

        filename=$(basename "$filePath")
        #echo "filename: " $filename

        #tmpDb=${filePath%.*}; 
        tmpDb=${filename%.sql.gz}
        echo "tmpDb: " $tmpDb

        ls -lh $filePath

        echo "mysql -h $mysql_host -u$mysql_user -p$mysql_pass -P $mysql_port -e \"CREATE DATABASE IF NOT EXISTS $tmpDb;\""
        mysql -h $mysql_host -u$mysql_user -p$mysql_pass -P $mysql_port -e "CREATE DATABASE IF NOT EXISTS $tmpDb;"

        echo "gunzip < $import_dir/$tmpDb.sql.gz | mysql -h $mysql_host -u$mysql_user -p$mysql_pass -P $mysql_port $tmpDb"
        gunzip < $import_dir/$tmpDb.sql.gz | mysqlcmd $tmpDb
    done

    notice say "Importing database $tmpDb completed!"

    # Stop here, ignore to import all other file
    exit
fi



echo "Do import all files: "
for filePath in $import_dir/*.sql.gz; do 
    echo -e "\n"
    #echo -e "\nProcessing $filePath ../"

    filename=$(basename "$filePath")
    #echo "filename: " $filename

    #tmpDb=${filePath%.*}; 
    tmpDb=${filename%.sql.gz}
    echo "tmpDb: " $tmpDb

    ls -lh $filePath

    echo "mysql -h $mysql_host -u$mysql_user -p$mysql_pass -P $mysql_port -e \"CREATE DATABASE IF NOT EXISTS $tmpDb;\""
    mysql -h $mysql_host -u$mysql_user -p$mysql_pass -P $mysql_port -e "CREATE DATABASE IF NOT EXISTS $tmpDb;"

    echo "gunzip < $import_dir/$tmpDb.sql.gz | mysql -h $mysql_host -u$mysql_user -p$mysql_pass -P $mysql_port $tmpDb"
    gunzip < $import_dir/$tmpDb.sql.gz | mysqlcmd $tmpDb

    notice say "Import $tmpDb completed!"
done

notice say "Import all files completed!"