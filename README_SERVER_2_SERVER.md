# Batch export and import Maria DB / MySQL

> TODO:  Make the script very simple for all user
> TODO: Change to use Go to write commandline app

The scenario: 
1. You will export db from machine `A`, 
2. download exported data to machine `B`, 
3. and import db into machine `B`

# 0. Put the BatchImportExportMysql into server

copy to machine B
```
sshpass -p 'xxxx' scp -r /Users/luatnd/Documents/Workspace/Document/BatchImportExportMysql xxx@xxx:/path/to/BatchImportExportMysql/
```


# 1. Export db from machine A
Run this on machine A:

Do config the mysql

```bash
cd /path/to/BatchImportExportMysql/
vim config.sh
```
```bash
# DB you wanna export
mysql_user="remote"
mysql_pass="remote"
mysql_host=127.0.0.1
mysql_port=3306

# Data dir you wanna export to: relative mean `export` folder relative to the exportDb.sh
export_dir="export"
```

You can ignore export for the `db30shine_rating` db by comment it out.
`vim exportDb.sh` 
```
dump-mysql-db-gzip db30shine_bill
dump-mysql-db-gzip db30shine_booking
dump-mysql-db-gzip db30shine_customer
dump-mysql-db-gzip db30shine_financial
dump-mysql-db-gzip db30shine_notification
dump-mysql-db-gzip db30shine_product
# dump-mysql-db-gzip db30shine_rating
dump-mysql-db-gzip db30shine_salon
dump-mysql-db-gzip db30shine_service
dump-mysql-db-gzip db30shine_staff
dump-mysql-db-gzip db30shine_user
```

Run export DB with data:
```bash
./exportDb.sh
```
Or export structure only:
```bash
./exportDb.sh --no-data
```

# 2. Download to machine B
From machine B command line:
```bash
sshpass -p 'A_SSH_PW' scp -r A_USER@A_IP:/path/to/BatchImportExportMysql/export/ /path/to/B_machine/Downloads/
```

# 3. Import 
From machine B command line:
Do config the mysql

```bash
cd /path/to/BatchImportExportMysql/
vim config.sh
```

```bash
# DB you wanna import to
mysql_user="remote"
mysql_pass="remote"
mysql_host=127.0.0.1
mysql_port=3306

# Data dir you wanna import from:
export_dir="/path/to/B_machine/Downloads/"
```

Run import:
```bash
./importDb.sh
```