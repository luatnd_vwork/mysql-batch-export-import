# Batch export and import Maria DB / MySQL

### Config
config-export.sh
```bash
# The source db that you export from
mysql_user="mobiletech"
mysql_pass="@@mobiletech"
mysql_host=13.228.49.166
mysql_port=3306

export_dir="data"
```

config-import.sh
```bash
# The destination db that you import to
mysql_user="root"
mysql_pass="test"
mysql_host=127.0.0.1
mysql_port=3307

import_dir="data"
```

### Do export
```bash
# dump some tables of db
./export-db.sh your_db table1,table2,table3,table4,table5
./export-db.sh tvn resume_applied,job

# dump whole db
./export-db.sh tvn
```

After that, data was dumped into ./data folder

```bash
$ ls -l ./data

total 94336
-rw-r--r--  1 luatnd  staff  47567686 Jul  2 11:10 tvn.sql.gz
```


### Do import the exported sql
```bash
# Import all db file in data folder
./import-db.sh

# OR: just import only tvn.sql.gz and vtn.sql.gz
./import-db.sh data/tvn.sql.gz data/vtn.sql.gz
```

Done


TODO: Support limit download only 1000 row, and allow configure that row number.