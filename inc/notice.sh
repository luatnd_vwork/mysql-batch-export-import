#!/bin/bash

function notice () {
	say=$1   # is mute/say
	msg=$2

	osascript << EOF
display notification "$msg" with title "Batch Export/Import Mysql" sound name "Submarine"
EOF

	if [ "$say" != "mute" ]; then
		osascript << EOF
say "$msg"
EOF
	fi
}


function runOsAppleScript() {
	osascript << EOF
$1
EOF
}