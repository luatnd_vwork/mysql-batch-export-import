#!/bin/bash

# alias mySingleDump="mysqldump --single-transaction -u$mysql_user -p$mysql_pass -h $mysql_host"
# Can not use alias in bash so we need a function 
#
# Usage: mysqldump-single-transaction "db30shine_product | gzip > export/db30shine_product.sql.gz"
#
function mysqldump-single-transaction () {
	# Is single table if table list doesn't contain comma
	isSingleTable=1
	if [[ $2 = *","* ]]; then
	  isSingleTable=0
	fi

	db=$1
	tables=${2//,/ }
	shift
	shift
	options=$@

	# echo $db
	# echo $tables
	# echo $options
	if [ "$isSingleTable" == 1 ]; then
		echo "Count $tables: "
		mysql -u$mysql_user -p$mysql_pass -h $mysql_host -P $mysql_port $db -e "SELECT count(*) as \`Count $tables\` FROM \`$tables\`;"
	fi
	

	if [ "$debug_only" == "1" ]; then
		echo "mysqldump --single-transaction -u$mysql_user -p$mysql_pass -h $mysql_host -P $mysql_port $options $db $tables | gzip > $export_dir/$db.sql.gz"
	elif [ "$sql_limit" != "0" ]; then
	  	mysqldump --single-transaction -u$mysql_user -p$mysql_pass -h $mysql_host -P $mysql_port $options $db $tables --where="1 LIMIT $sql_limit" | gzip > $export_dir/$db.sql.gz
  	else
  		mysqldump --single-transaction -u$mysql_user -p$mysql_pass -h $mysql_host -P $mysql_port $options $db $tables | gzip > $export_dir/$db.sql.gz
	fi
}

function mysqlcmd () {
	if [ "$debug_only" == "1" ]; then
		echo "mysql -h $mysql_host -u$mysql_user -p$mysql_pass -P $mysql_port $@"
	else
	  	mysql -h $mysql_host -u$mysql_user -p$mysql_pass -P $mysql_port $@ 
	fi
}

function dump-mysql-db-gzip () {
	mysqldump-single-transaction $@
}
