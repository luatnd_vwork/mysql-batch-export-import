#!/bin/bash

# Modify your database config in config.sh before run ./$export_dirDb.sh
source config-export.sh
source inc/mysql_utils.sh
source inc/notice.sh

export_db=$1
export_tables=$2
shift 
shift
mysqldump_option=$@

# ./export-db.sh tvn resume_applied,job [--mysqldump-option-here value]
dump-mysql-db-gzip $export_db $export_tables $mysqldump_option

# dump-mysql-db-gzip db30shine_booking $mysqldump_option
# dump-mysql-db-gzip db30shine_customer $mysqldump_option
# dump-mysql-db-gzip db30shine_financial $mysqldump_option
# dump-mysql-db-gzip db30shine_notification $mysqldump_option
# dump-mysql-db-gzip db30shine_product $mysqldump_option
# dump-mysql-db-gzip db30shine_rating $mysqldump_option
# dump-mysql-db-gzip db30shine_salon $mysqldump_option
# dump-mysql-db-gzip db30shine_service $mysqldump_option
# dump-mysql-db-gzip db30shine_staff $mysqldump_option
# dump-mysql-db-gzip db30shine_user $mysqldump_option


notice say "Export completed!"